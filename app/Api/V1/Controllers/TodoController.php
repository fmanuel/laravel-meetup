<?php

namespace Api\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\TodoRequest;
use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TodoController
 *
 * @package Api\Controllers
 *
 * @property \App\User $user
 */
class TodoController extends Controller {

    private $user;

    public function __construct() {
        $this->user = Auth::guard('api')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(
            $this->user->todos()
                ->orderBy('priority')
                ->with('tags')
                ->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TodoRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function store(TodoRequest $request) {
        $todo = Todo::make($request->all());

        if (!$this->user->todos()->save($todo)) {
            \Log::error("Could not save Todo! Received data: " . json_encode($request->all()));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The new todo item could not be saved!");
        }

        return response()->json($todo->fresh()->load('tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo $todo
     *
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function show(Todo $todo) {
        $this->checkPermission($todo);

        return response()->json($todo->load('tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TodoRequest $request
     * @param  \App\Todo  $todo
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function update(TodoRequest $request, Todo $todo) {
        $this->checkPermission($todo);

        $todo->fill($request->all());
        if (! $todo->save() ) {
            \Log::error("Could not save Todo! Received data: " . json_encode($request->all()));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The todo item could not be saved!");
        }

        return response()->json($todo->fresh()->load('tags'));
    }

    /**
     * @param Request $request
     * @param Todo    $todo
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function toggleChecked(Request $request, Todo $todo) {
        $this->checkPermission($todo);

        $todo->checked = !$todo->checked;
        if (! $todo->save() ) {
            \Log::error("Could not save Todo! Received data: " . json_encode($request->all()));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The todo item could not be saved!");
        }

        return response()->json($todo->fresh()->load('tags'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo $todo
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Todo $todo) {
        $this->checkPermission($todo);

        if (! $todo->delete() ) {
            \Log::error("Could not delete Todo!");
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The todo item could not be deleted!");
        }

        return response()->json($todo);
    }

    /**
     * @param Todo $todo
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function checkPermission(Todo &$todo) {

        if (!$todo || $todo->user_id !== $this->user->id) {
            abort(Response::HTTP_FORBIDDEN, "You have no access to this todo!");
        }

    }
}
