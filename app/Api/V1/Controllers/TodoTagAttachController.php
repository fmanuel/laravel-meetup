<?php

namespace Api\Controllers;

use App\Http\Controllers\Controller;
use App\Tag;
use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TodoTagAttachController
 *
 * @package Api\Controllers
 *
 * @property \App\User $user
 */
class TodoTagAttachController extends Controller {

    private $user;

    public function __construct() {
        $this->user = Auth::guard('api')->user();
    }

    /**
     * @param Request $request
     * @param Todo    $todo
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function __invoke(Request $request, Todo $todo) {
        $this->checkPermission($todo);

        $tag = Tag::findOrFail($request->get("tag_id"));

        $todo->tags()->syncWithoutDetaching($tag);

        return response($todo->fresh()->load('tags'));
    }

    /**
     * @param Request $request
     * @param Todo    $todo
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function detachTag(Request $request, Todo $todo) {
        $this->checkPermission($todo);

        $tag = Tag::findOrFail($request->get("tag_id"));

        if (! $todo->tags()->detach($tag) ) {
            \Log::error("Could not detach tag from todo ({$todo->id})! Received data: " . json_encode($request->all()));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The tag could not be detached from the todo!");
        }

        return response($todo->fresh()->load('tags'));
    }

    /**
     * @param Todo $todo
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function checkPermission(Todo &$todo) {

        if (!$todo || $todo->user_id !== $this->user->id) {
            abort(Response::HTTP_FORBIDDEN, "You have no access to this todo!");
        }

    }
}
