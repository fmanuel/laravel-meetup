<?php

namespace Api\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Tag;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response(Tag::withCount('todos')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TagRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function store(TagRequest $request) {
        $tag = Tag::make($request->all());

        if (!$tag->save()) {
            \Log::error("Could not save Tag! Received data: " . json_encode($request->all()));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The new tag item could not be saved!");
        }

        return response($tag);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag) {
        return response($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TagRequest $request
     * @param  \App\Tag  $tag
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function update(TagRequest $request, Tag $tag) {
        $tag->fill($request->all());

        if (!$tag->save()) {
            \Log::error("Could not save Tag! Received data: " . json_encode($request->all()));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The new tag item could not be saved!");
        }

        return response($tag->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag $tag
     *
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Exception
     */
    public function destroy(Tag $tag) {
        if (!$tag->delete()) {
            \Log::error("Could not delete Todo!");
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "The todo item could not be deleted!");
        }

        return response()->json($tag);
    }

}
