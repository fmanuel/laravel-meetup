<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TodoRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $labelRules = $this->isMethod('PUT') ? 'sometimes|required|string|max:255' : 'required|string|max:255';

        return [
            'checked' => 'sometimes|required|boolean',
            'label'   => $labelRules,
            'priority' => 'sometimes|required|integer',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'checked.boolean' => 'A teljesítettség értéke érvénytelen!',
            'label.required'  => 'Címke megadása kötelező!',
            'label.string'    => 'A címkének szöveg típusúnak kell lennie!',
            'label.max'       => 'A címke nem lehet hosszabb 255 karakternél!',
            'priority.integer' => 'A sorszámnak egész számnak kell lennie!',
        ];
    }

}
