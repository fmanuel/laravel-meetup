<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TagRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $labelRules = $this->isMethod('PUT') ? 'sometimes|required|string|max:255' : 'required|string|max:255';

        return [
            'label'   => $labelRules,
            'color' => 'sometimes|required|string|size:7',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'label.required'  => 'Címke megadása kötelező!',
            'label.string'    => 'A címkének szöveg típusúnak kell lennie!',
            'label.max'       => 'A címke nem lehet hosszabb 255 karakternél!',
            'color.required' => 'Színkód megadása kötelező!',
            'color.string' => 'A színkódnak szöveg típusúnak kell lennie!',
            'color.size' => 'A színkód helyes formátuma: "#HHHHHH"',
        ];
    }
}
