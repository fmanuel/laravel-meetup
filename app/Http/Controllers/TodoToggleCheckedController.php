<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TodoToggleCheckedController extends Controller {
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Todo                      $todo
     *
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function __invoke(Request $request, Todo $todo) {
        $this->checkPermission($request, $todo);

        $todo->checked = !$todo->checked;
        $todo->save();

        return redirect('home');
    }

    /**
     * @param Request $request
     * @param Todo    $todo
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function checkPermission(Request $request, Todo &$todo) {

        if (!$todo || $todo->user_id !== $request->user()->id) {
            abort(Response::HTTP_FORBIDDEN, "Nincs jogosultságod ehhez a teendőhöz!");
        }

    }
}
