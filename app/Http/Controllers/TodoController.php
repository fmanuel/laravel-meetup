<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoRequest;
use App\Tag;
use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TodoController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $tags = Tag::all();

        return view('todo.create', ['tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TodoRequest $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function store(TodoRequest $request) {
        /* @var Todo $todo */
        $todo = Todo::make($request->all());

        if (!$request->user()->todos()->save($todo)) {
            \Log::error("Could not save Todo! Received data: " . json_encode($request->all()));
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, "Az új teendő nem volt menthető!");
        }

        $selectedTags = $request->get('tags');
        if ($selectedTags && count($selectedTags)) {
            $todo->tags()->attach($selectedTags);
        }

        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo $todo
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo $todo
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Todo                $todo
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo $todo
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo) {
        //
    }
}
