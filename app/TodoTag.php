<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\TodoTag
 *
 * @property int $id
 * @property int $todo_id
 * @property int $tag_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Tag $tag
 * @property-read \App\Todo $todo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag whereTodoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TodoTag whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TodoTag extends Pivot {

    public function todo() {
        return $this->belongsTo(Todo::class);
    }

    public function tag() {
        return $this->belongsTo(Tag::class);
    }

}
