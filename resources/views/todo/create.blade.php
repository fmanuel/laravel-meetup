@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Új teendő</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ route('todo-store') }}"
                          method="POST"
                          class="needs-validation @if($errors->any()) was-validated @endif"
                          novalidate
                    >
                        @csrf

                        <div class="form-row my-3">
                            <div class="col-12">
                                <label for="label">Szöveg</label>
                                <input type="text"
                                       class="form-control"
                                       name="label"
                                       id="label"
                                       required
                                />
                                <div class="invalid-feedback">
                                @foreach($errors->get('label') as $message)
                                    {{ $message }} <br>
                                @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-row my-3">
                            <div class="col-12">
                                <label for="priority">Prioritás</label>
                                <input type="number"
                                       class="form-control"
                                       name="priority"
                                       id="priority"
                                       value="1"
                                />
                                <div class="invalid-feedback">
                                @foreach($errors->get('priority') as $message)
                                    {{ $message }} <br>
                                @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-row my-3">
                            <div class="col-12">
                                <label for="tags">Címkék</label>
                                <div class="tag-check-list">
                                    @foreach($tags as $tag)
                                        <div class="form-check">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   value="{{$tag->id}}"
                                                   id="tags-{{$tag->id}}"
                                                   name="tags[]"
                                            />
                                            <label class="form-check-label" for="tags-{{$tag->id}}" style="color: {{$tag->color}}">
                                                {{$tag->label}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="invalid-feedback">
                                @foreach($errors->get('tags') as $message)
                                    {{ $message }} <br>
                                @endforeach
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Mentés</button>
                                <a href="{{ route('home') }}" class="btn btn-outline-secondary">Mégsem</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
