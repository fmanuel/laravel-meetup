@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Teendők</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="my-2 actions">
                        <a href="{{ route('todo-create') }}" class="btn btn-outline-success">Új teendő</a>
                        <a href="{{ route('tags.index') }}" class="btn btn-outline-primary float-right">Címkék</a>
                    </div>

                    <div class="list-group">
                        @foreach($todoList as $todo)
                        <a href="{{ route('toggle-checked', ['todo' => $todo->id]) }}" class="list-group-item list-group-item-action @if($todo->checked) checked @endif">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{ $todo->label }}</h5>
                                @if($todo->checked) <small class="text-success">Kész</small> @endif
                            </div>
                            <div class="tag-list">
                                @foreach($todo->tags as $tag)
                                <small class="tag" style="color: {{ $tag->color }}">{{ $tag->label }}</small>
                                @endforeach
                            </div>
                        </a>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
