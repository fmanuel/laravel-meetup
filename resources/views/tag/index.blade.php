@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Címkék</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="my-2 actions">
                        <a href="{{ route('tags.create') }}" class="btn btn-outline-success">Új címke</a>
                        <a href="{{ route('home') }}" class="btn btn-outline-primary float-right">Teendők</a>
                    </div>

                    <div class="list-group">
                        @foreach($tags as $tag)
                        <a href="{{ route('tags.edit', ['tag' => $tag->id]) }}" class="list-group-item list-group-item-action">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{ $tag->label }} ({{ $tag->color }})</h5>
                                <small style="display: inline-block; width: 20px; height: 20px; overflow: hidden; background: {{ $tag->color }};">&nbsp;</small>
                            </div>
                        </a>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
