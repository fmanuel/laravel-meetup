<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function ($router) {
    Route::get('/todos/new', 'TodoController@create')->name('todo-create');
    Route::post('/todos', 'TodoController@store')->name('todo-store');
    Route::get('/todos/{todo}/toggle-checked', 'TodoToggleCheckedController')->name('toggle-checked');

    Route::resource('tags', 'TagController');
});
