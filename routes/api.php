<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get(
//    '/user',
//    function (Request $request) {
//        return $request->user();
//    }
//);

Route::group(
    [
        'middleware' => 'api',
        'prefix'     => 'auth',
    ],
    function ($router) {

        Route::post('signup', \App\Http\Controllers\Auth\RegisterController::class . '@register');
        Route::post('login', \Api\Controllers\AuthController::class . '@login');

        Route::group(
            ['middleware' => 'auth:api'],
            function ($router) {

                Route::post('logout', \Api\Controllers\AuthController::class . '@logout');
                Route::post('refresh', \Api\Controllers\AuthController::class . '@refresh');
                Route::post('me', \Api\Controllers\AuthController::class . '@me');

            }
        );

    }
);

Route::group(
    [
        'middleware' => ['api', 'auth:api'],
    ],
    function ($router) {

        Route::apiResource('todos', \Api\Controllers\TodoController::class);
        Route::put('todos/{todo}/toggle-checked', \Api\Controllers\TodoController::class . '@toggleChecked');

        Route::apiResource('tags', \Api\Controllers\TagController::class);

        Route::post('todos/{todo}/attach-tag', \Api\Controllers\TodoTagAttachController::class);
        Route::delete('todos/{todo}/detach-tag', \Api\Controllers\TodoTagAttachController::class . '@detachTag');

    }
);
