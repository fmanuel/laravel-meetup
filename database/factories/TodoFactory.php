<?php

use Faker\Generator as Faker;

$factory->define(
    App\Todo::class,
    function (Faker $faker) {
        return [
            'label'   => $faker->words(4, true),
            'checked' => false,
            'priority' => 1,
        ];
    }
);
