<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoTagTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(
            'todo_tag',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('todo_id');
                $table->unsignedInteger('tag_id');
                $table->timestamps();

                $table->unique(['todo_id', 'tag_id']);

                $table->foreign('todo_id')
                    ->references('id')->on('todos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

                $table->foreign('tag_id')
                    ->references('id')->on('tags')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('todo_tag');
    }
}
