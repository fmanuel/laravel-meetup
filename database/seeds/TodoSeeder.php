<?php

use Illuminate\Database\Seeder;

class TodoSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $user = \App\User::inRandomOrder()->first();
        factory(\App\Todo::class, 7)->create(['user_id' => $user->id])->each(
            function ($todo) {
                if (!rand(0, 1)) {
                    $totalTagCount = \App\Tag::count();
                    $tagIds = \App\Tag::inRandomOrder()->take(rand(1, $totalTagCount))->pluck('id');
                    $todo->tags()->attach($tagIds);
                }
            }
        );
    }
}
